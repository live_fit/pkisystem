package cn.accjiyun.pki.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/6/1.
 */
@Entity
public class Tax implements Serializable {
    private int id;
    private Double income;
    private Double tax;
    private byte status;
    private Timestamp createTime;
    private String attachUrl;
    private Timestamp verifyTime;
    private Packets packetsById;
    private SystemUser systemUserByUserId;
    private TaxType taxTypeByTaxTypeId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "income", nullable = true, precision = 0)
    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    @Basic
    @Column(name = "tax", nullable = true, precision = 0)
    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "attach_url", nullable = false, length = 255)
    public String getAttachUrl() {
        return attachUrl;
    }

    public void setAttachUrl(String attachUrl) {
        this.attachUrl = attachUrl;
    }

    @Basic
    @Column(name = "verify_time", nullable = true)
    public Timestamp getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Timestamp verifyTime) {
        this.verifyTime = verifyTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tax tax1 = (Tax) o;

        if (id != tax1.id) return false;
        if (status != tax1.status) return false;
        if (income != null ? !income.equals(tax1.income) : tax1.income != null) return false;
        if (tax != null ? !tax.equals(tax1.tax) : tax1.tax != null) return false;
        if (createTime != null ? !createTime.equals(tax1.createTime) : tax1.createTime != null) return false;
        if (attachUrl != null ? !attachUrl.equals(tax1.attachUrl) : tax1.attachUrl != null) return false;
        if (verifyTime != null ? !verifyTime.equals(tax1.verifyTime) : tax1.verifyTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (income != null ? income.hashCode() : 0);
        result = 31 * result + (tax != null ? tax.hashCode() : 0);
        result = 31 * result + (int) status;
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (attachUrl != null ? attachUrl.hashCode() : 0);
        result = 31 * result + (verifyTime != null ? verifyTime.hashCode() : 0);
        return result;
    }

    @OneToOne(mappedBy = "taxById")
    public Packets getPacketsById() {
        return packetsById;
    }

    public void setPacketsById(Packets packetsById) {
        this.packetsById = packetsById;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public SystemUser getSystemUserByUserId() {
        return systemUserByUserId;
    }

    public void setSystemUserByUserId(SystemUser systemUserByUserId) {
        this.systemUserByUserId = systemUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "tax_type_id", referencedColumnName = "id")
    public TaxType getTaxTypeByTaxTypeId() {
        return taxTypeByTaxTypeId;
    }

    public void setTaxTypeByTaxTypeId(TaxType taxTypeByTaxTypeId) {
        this.taxTypeByTaxTypeId = taxTypeByTaxTypeId;
    }
}
