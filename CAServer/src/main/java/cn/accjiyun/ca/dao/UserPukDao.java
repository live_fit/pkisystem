package cn.accjiyun.ca.dao;

import cn.accjiyun.ca.common.dao.BaseDao;
import cn.accjiyun.ca.common.entity.PageModel;
import cn.accjiyun.ca.common.entity.QueryEntity;
import cn.accjiyun.ca.entity.UserPuk;

import java.util.List;

/**
 * Created by jiyun on 2017/5/23.
 */
public interface UserPukDao extends BaseDao<UserPuk> {

    /**
     * 创建用户公钥证书
     * @param userPuk 用户公钥证书实体
     * @return 用户公钥证书ID
     */
    public int createUserPuk(UserPuk userPuk);

    /**
     * 通过用户公钥证书ID数组删除用户公钥证书
     * @param userPukIds 用户公钥证书ID数组
     */
    public void deleteUserPuk(int[] userPukIds);

    /**
     * 通过用户公钥证书ID查询用户公钥证书信息
     * @param userPukId 庭成员ID
     * @return 用户公钥证书实体
     */
    public UserPuk queryUserPukById(int userPukId);

    /**
     * 分页查询用户公钥证书信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 用户公钥证书实体列表
     */
    public List<UserPuk> queryUserPukPage(QueryEntity queryEntity, PageModel<UserPuk> model);

    /**
     * 更新用户公钥证书信息
     * @param userPuk 用户公钥证书实体
     */
    public void updateUserPuk(UserPuk userPuk);

    /**
     * 获取用户公钥证书总数
     * @return 用户公钥证书总数
     */
    public long queryAllUserPukCount();

    /**
     * 利用hql语句查找用户公钥证书信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 用户公钥证书实体列表
     */
    public List<UserPuk> createQuery(final String hql, final Object[] queryParams);
}