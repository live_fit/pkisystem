layui.define(['jquery', 'tags', 'element', 'layedit', 'jqform', 'upload', 'jqdate', 'layer', 'ajax'], function (exports) {
    var $ = layui.jquery,
        ajax = layui.ajax,
        layedit = layui.layedit,
        box = "",
        form = layui.jqform,
        tags = layui.tags,
        laydate = layui.laydate;
    form.set({
        "blur": true,
        "form": "#form1",
        "change": true
    }).init();

    form.setTax = function (ret, options, that) {
        var income = $("#income").val();
        if (undefined != income) {
            $("#tax").value = ret.taxRate * income;
        }
    }

    //自定义
    form.verify({
        username: function (value) {
            if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
                return '文章标题不能有特殊字符';
            }
            if (/(^\_)|(\__)|(\_+$)/.test(value)) {
                return '文章标题首尾不能出现下划线\'_\'';
            }
            if (/^\d+\d+$/.test(value)) {
                return '文章标题不能全为数字';
            }
        },
        pass: [
            /^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'
        ],
        content: function (value) {
            layedit.sync(editIndex);
            return;
        },
        confirmPass: function (value) {
            var password = $("#password").val();
            if (password != value) {
                return '两次输入的密码不一致';
            }
        },
        validateLoginName: function (value) {
            $.ajax({
                url: '/admin/system/validateLoginName?loginName=' + value,
                type: 'post',
                dataType: 'json',
                data: "",
                async : false,
                success: function (result) {
                    if (result.status != 200) {
                        return "用户名已存在！";
                    }
                }
            });
        }
    });
    tags.init();

    var dirPath = "";
    $("input[name='file']").each(function(){
        dirPath = $(this).attr("id");
    })

    //上传文件设置
    layui.upload({
        url: '/admin/upload/file?param=' + dirPath,
        before: function (input) {
            box = $(input).parent('form').parent('div').parent('.layui-input-block');
            if (box.next('div').length > 0) {
                box.next('div').html('<div class="imgbox"><p>上传中...</p></div>');
            } else {
                box.after('<div class="layui-input-block"><div class="imgbox"><p>上传中...</p></div></div>');
            }
        },
        success: function (res) {
            if (res.status == 200) {
                if ("jpg,gif,png,jpeg".indexOf(res.ext) >= 0) {
                    box.next('div').find('div.imgbox').html('<img src="' + res.url + '" name="imageUrl" alt="暂无" class="img-thumbnail">');
                } else {
                    box.next('div').find('p').html('上传成功');
                }
                box.find('input[type=hidden]').val(res.url);
                form.check(box.find('input[type=hidden]'));
            } else {
                box.next('div').find('p').html('上传失败...')
            }
        }
    });

    //富文本框
    layedit.set({
        uploadImage: {
            url: '/admin/upload/file?param=article'
        }
    });
    var editIndex = layedit.build('content');


    exports('myform', {});
});