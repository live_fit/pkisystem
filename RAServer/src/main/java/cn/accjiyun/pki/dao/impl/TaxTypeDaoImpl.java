package cn.accjiyun.pki.dao.impl;

import cn.accjiyun.pki.common.dao.DaoSupport;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.dao.TaxTypeDao;
import cn.accjiyun.pki.entity.TaxType;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/5/18.
 */
@Repository("taxTypeDao")
public class TaxTypeDaoImpl extends DaoSupport<TaxType> implements TaxTypeDao {
    /**
     * 创建税务类型
     *
     * @param taxType 税务类型实体
     * @return 税务类型ID
     */
    @Override
    public int createTaxType(TaxType taxType) {
        save(taxType);
        return taxType.getId();
    }

    /**
     * 通过税务类型ID数组删除税务类型
     *
     * @param taxTypeIds 税务类型ID数组
     */
    @Override
    public void deleteTaxTypeById(int[] taxTypeIds) {
        for (int i = 0; i < taxTypeIds.length; i++) {
            delete(taxTypeIds[i]);
        }
    }

    /**
     * 通过税务类型ID查询税务类型信息
     *
     * @param taxTypeId 庭成员ID
     * @return 税务类型实体
     */
    @Override
    public TaxType queryTaxTypeById(int taxTypeId) {
        return get(taxTypeId);
    }

    /**
     * 通过税务类型名称查询对应ID
     *
     * @param roleName 税务类型名称
     * @return 税务类型ID
     */
    @Override
    public int queryTaxTypeIdByName(String roleName) {
        Query query = getSession().createQuery("select ty.id from TaxType as ty where ty.typeName=?");
        Object[] queryParams = {roleName};
        setQueryParams(query, queryParams);
        return (Integer) (query.list().get(0));
    }

    /**
     * 分页查询税务类型信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 税务类型实体列表
     */
    @Override
    public List<TaxType> queryTaxTypePage(QueryEntity queryEntity, PageModel<TaxType> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取税务类型总数
     *
     * @return 税务类型总数
     */
    @Override
    public long queryAllTaxTypeCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找税务类型信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 税务类型实体列表
     */
    @Override
    public List<TaxType> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 更新税务类型信息
     *
     * @param taxType 税务类型实体
     */
    @Override
    public void updateTaxType(TaxType taxType) {
        saveOrUpdate(taxType);
    }
}
