package cn.accjiyun.pki.dao.impl;

import cn.accjiyun.pki.common.dao.DaoSupport;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.dao.PacketsDao;
import cn.accjiyun.pki.entity.Packets;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/5/18.
 */
@Repository("packetsDao")
public class PacketsDaoImpl extends DaoSupport<Packets> implements PacketsDao {
    /**
     * 创建数据包
     *
     * @param packets 数据包实体
     * @return 数据包ID
     */
    @Override
    public int createPackets(Packets packets) {
        save(packets);
        return packets.getId();
    }

    /**
     * 通过数据包ID数组删除数据包
     *
     * @param packetsIds 数据包ID数组
     */
    @Override
    public void deletePackets(int[] packetsIds) {
        for (int i = 0; i < packetsIds.length; i++) {
            delete(packetsIds[i]);
        }
    }

    /**
     * 通过数据包ID查询数据包信息
     *
     * @param packetsId 庭成员ID
     * @return 数据包实体
     */
    @Override
    public Packets queryPacketsById(int packetsId) {
        return get(packetsId);
    }

    /**
     * 分页查询数据包信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 数据包实体列表
     */
    @Override
    public List<Packets> queryPacketsPage(QueryEntity queryEntity, PageModel<Packets> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取数据包总数
     *
     * @return 数据包总数
     */
    @Override
    public long queryAllPacketsCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找数据包信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 数据包实体列表
     */
    @Override
    public List<Packets> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 更新数据包信息
     *
     * @param packets 数据包实体
     */
    @Override
    public void updatePackets(Packets packets) {
        saveOrUpdate(packets);
    }
}
