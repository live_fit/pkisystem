package cn.accjiyun.ca.service;

import cn.accjiyun.ca.common.constants.CommonConstants;
import cn.accjiyun.ca.common.entity.PageModel;
import cn.accjiyun.ca.common.entity.QueryEntity;
import cn.accjiyun.ca.common.utils.DateUtils;
import cn.accjiyun.ca.dao.UserPukDao;
import cn.accjiyun.ca.entity.CAConfig;
import cn.accjiyun.ca.entity.UserPuk;
import cn.accjiyun.ca.security.BaseCert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jiyun on 2017/5/23.
 */

@Service("userPukService")
public class UserPukServiceImpl implements UserPukService {

    @Autowired
    private UserPukDao userPukDao;


    /**
     * 生成证书
     *
     * @param userPukId 用户公钥证书ID
     * @param caConfig 证书配置属性
     * @param certPath 生成路径
     */
    @Override
    public void generateCert(int userPukId, CAConfig caConfig, String certPath) {
        new BaseCert().generateCert(certPath, caConfig);
    }

    /**
     * 创建用户公钥证书
     *
     * @param userPuk 用户公钥证书实体
     * @return 用户公钥证书ID
     */
    @Override
    public int createUserPuk(UserPuk userPuk) {
        return userPukDao.createUserPuk(userPuk);
    }

    /**
     * 通过用户公钥证书ID数组删除用户公钥证书
     *
     * @param userPukIds 用户公钥证书ID数组
     */
    @Override
    public void deleteUserPuk(int[] userPukIds) {
        userPukDao.deleteUserPuk(userPukIds);
    }

    /**
     * 通过用户公钥证书ID查询用户公钥证书信息
     *
     * @param userPukId 庭成员ID
     * @return 用户公钥证书实体
     */
    @Override
    public UserPuk queryUserPukById(int userPukId) {
        return userPukDao.queryUserPukById(userPukId);
    }

    /**
     * 分页查询用户公钥证书信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 用户公钥证书实体列表
     */
    @Override
    public List<UserPuk> queryUserPukPage(QueryEntity queryEntity, PageModel<UserPuk> model) {
        return userPukDao.queryUserPukPage(queryEntity, model);
    }

    /**
     * 更新用户公钥证书信息
     *
     * @param userPuk 用户公钥证书实体
     */
    @Override
    public void updateUserPuk(UserPuk userPuk) {
        userPukDao.updateUserPuk(userPuk);
    }

    /**
     * 获取用户公钥证书总数
     *
     * @return 用户公钥证书总数
     */
    @Override
    public long queryAllUserPukCount() {
        return userPukDao.queryAllUserPukCount();
    }

    /**
     * 利用hql语句查找用户公钥证书信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 用户公钥证书实体列表
     */
    @Override
    public List<UserPuk> createQuery(String hql, Object[] queryParams) {
        return userPukDao.createQuery(hql, queryParams);
    }
}
