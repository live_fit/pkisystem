package cn.accjiyun.ca.controller;

import cn.accjiyun.ca.common.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by jiyun on 2017/5/24.
 */
@Controller
public class WebFrontController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(WebFrontController.class);
    private static String indexPage = getViewPath("/index");
    /**
     * 首页获取网站首页数据
     */
    @RequestMapping("/index")
    public ModelAndView getIndexpage() {
        ModelAndView model = new ModelAndView();
        model.setViewName(indexPage);
        return model;
    }

}
