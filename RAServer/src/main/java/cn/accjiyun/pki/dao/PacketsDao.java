package cn.accjiyun.pki.dao;

import cn.accjiyun.pki.common.dao.BaseDao;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.entity.Packets;

import java.util.List;

/**
 * Created by jiyun on 2017/5/18.
 */
public interface PacketsDao extends BaseDao<Packets> {

    /**
     * 创建数据包
     * @param packets 数据包实体
     * @return 数据包ID
     */
    public int createPackets(Packets packets);

    /**
     * 通过数据包ID数组删除数据包
     * @param packetsIds 数据包ID数组
     */
    public void deletePackets(int[] packetsIds);

    /**
     * 通过数据包ID查询数据包信息
     * @param packetsId 庭成员ID
     * @return 数据包实体
     */
    public Packets queryPacketsById(int packetsId);

    /**
     * 分页查询数据包信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 数据包实体列表
     */
    public List<Packets> queryPacketsPage(QueryEntity queryEntity, PageModel<Packets> model);

    /**
     * 更新数据包信息
     * @param packets 数据包实体
     */
    public void updatePackets(Packets packets);

    /**
     * 获取数据包总数
     * @return 数据包总数
     */
    public long queryAllPacketsCount();

    /**
     * 利用hql语句查找数据包信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 数据包实体列表
     */
    public List<Packets> createQuery(final String hql, final Object[] queryParams);
}