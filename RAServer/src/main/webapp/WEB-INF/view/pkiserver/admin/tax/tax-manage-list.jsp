<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!--头部搜索-->
            <section class="panel panel-padding">
                <form class="layui-form" action="/admin/tax/getTaxList?pageNo=1">
                    <div class="layui-form">
                        <div class="layui-inline">
                            <select name="taxTypeId" jq-verify="required" jq-error="请选择税务类型" lay-filter="ajax"
                                    data-params='{"url": "/admin/tax/getTaxRate","loading":"false", "confirm":"true", "complete":"search"}'>
                                <option value="">请选择</option>
                                <c:if test="${not empty taxTypeList }">
                                    <c:forEach var="taxType" items="${taxTypeList}" varStatus="status">
                                        <option value="${taxType.id}">${taxType.typeName}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input start-date" name="start_date" placeholder="开始日">
                            </div>
                            <div class="layui-input-inline">
                                <input class="layui-input end-date" name="end_date" placeholder="截止日">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input" name="keyword" placeholder="用户姓名">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button lay-submit class="layui-btn" lay-filter="search">查找</button>
                        </div>
                    </div>
                </form>
            </section>

            <!--列表-->
            <section class="panel panel-padding">
                <div class="group-button">
                    <button class="layui-btn layui-btn-small layui-btn-danger ajax-all" data-name="id"
                            data-params='{"url": "/admin/tax/delete","loading":"true"}'>
                        <i class="iconfont">&#xe626;</i> 删除
                    </button>
                </div>
                <div id="list" class="layui-form"></div>

                <div class="text-right" id="page"></div>
            </section>
        </div>
    </div>
</div>

<div class="add-subcat">
    <form id="form1" class="layui-form layui-form-pane" data-name="ordersData" data-tpl="list-tpl"
          data-render="true" action="/admin/tax/updateTax" method="post">

        <div class="layui-form-item">
            <label class="layui-form-label">收入</label>
            <div class="layui-input-inline">
                <input type="text" disabled="disabled" name="income" jq-verify="required|number" jq-error="请输入正确的金额"  placeholder="金额" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">税务类型</label>
            <div class="layui-input-inline">
                <select name="taxTypeId" disabled="disabled" jq-verify="required" jq-error="请选择税务类型">
                    <option value="">请选择</option>
                    <c:if test="${not empty taxTypeList }">
                        <c:forEach var="taxType" items="${taxTypeList}" varStatus="status">
                            <option value="${taxType.id}">${taxType.typeName}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </div>

        <%--<div class="layui-form-item">--%>
        <%--<label class="layui-form-label">税费</label>--%>
        <%--<div class="layui-input-inline">--%>
        <%--<input type="text" name="tax" jq-verify="required|number" jq-error="请输入正确的税费"  placeholder="税费" autocomplete="off" class="layui-input ">--%>
        <%--</div>--%>
        <%--</div>--%>

        <div class="layui-form-item">
            <label class="layui-form-label">上传图片</label>
            <div class="layui-input-block">
                <input type="file" name="file" disabled="disabled" lay-type="file" class="layui-upload-file" id="attach">
                <input type="hidden" name="attachUrl" jq-verify="required" jq-error="请上传图片"
                       error-id="img-error">
                <p id="file-error" class="error"></p>
            </div>
            <div class="layui-input-block">
                <div class="imgbox">
                    <img name="attachUrl" src="/static/admin/images/logo-color.png" alt="暂无" class="img-thumbnail">
                </div>
            </div>
        </div>

        <label class="layui-form-label">申报内容</label>
        <div class="layui-form-item layui-form-text">
            <div class="layui-input-block">
                <textarea name="content" disabled="disabled" jq-verify="content" id="content"></textarea>
            </div>
        </div>

    </form>
</div>

</body>
<%@include file="/WEB-INF/layouts/admin/tax-manage.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('list');
    layui.use('myform');

    function test(ret, options, $) {
        console.log($);
        alert("这是自定义的ajax返回方法，可以用$哦");
    }
</script>

</html>