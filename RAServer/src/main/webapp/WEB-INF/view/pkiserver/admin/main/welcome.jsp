<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <section class="panel" style="border-width: 25px;">
                <h1>
                    <span style="font-size: 28px;color: #FF760B;padding-bottom: 15px;">
                    ${systemUser.username}，
                    欢迎您！&nbsp;&nbsp;&nbsp;
                    </span>
                    您的身份是：<span style="color: #FF760B;">${role.roleName}</span>
                </h1>
                <hr/>
                <br/>
                <span style="font-size: 20px">
                    <i class="layui-icon" style="color: #1E9FFF;">&#xe63b;</i>&nbsp;&nbsp;
                    ${systemUser.mobile}
                </span>
                <br/>
                <br/>
                <p>点击快捷入口即可管理操作！</p>
            </section>
        </div>
    </div>

    <%--管理员快捷操作入口--%>
    <c:if test="${roleId == 99}">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
            <section class="panel">
                <div class="symbol bgcolor-yellow"><i class="iconfont">&#xe674;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/system/userAdd"
                       data-parent="true" data-title="添加用户"> <i class="iconfont " data-icon='&#xe674;'></i>
                        <span style="font-size: 18px;">添加用户</span>
                    </a>
                </div>
            </section>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
            <section class="panel">
                <div class="symbol bgcolor-blue"><i class="iconfont">&#xe681;</i>
                </div>
                <div class="value tab-menu">
                    <a href="javascript:;" data-url="/admin/system/userList"
                       data-parent="true" data-title="社员列表"><i class="iconfont " data-icon='&#xe681;'></i>
                        <h1>${systemUserCount}</h1>
                    </a>
                    <a href="javascript:;" data-url="/admin/system/userList" data-parent="true"
                       data-title="用户列表">
                        <i class="iconfont" data-icon='&#xe681;'></i><span>用户列表</span>
                    </a>
                </div>
            </section>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
            <section class="panel">
                <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe63f;</i>
                </div>
                <div class="value tab-menu">
                    <a href="javascript:;" data-url="/admin/tax/initManagerTaxList"
                       data-parent="true" data-title="税务列表"> <i class="iconfont " data-icon='&#xe63f;'></i>
                        <h1>${taxCount}</h1>
                    </a>
                    <a href="javascript:;" data-url="/admin/tax/initManagerTaxList"
                       data-parent="true" data-title="税务列表">
                        <i class="iconfont " data-icon='&#xe63f;'></i><span>税务列表</span></a>
                </div>
            </section>
        </div>
    </div>
    </c:if>

    <%--承办人快捷操作入口--%>
    <c:if test="${roleId == 1}">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
        <section class="panel">
            <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe63f;</i>
            </div>
            <div class="value tab-menu" style="margin-top: 2em">
                <a href="javascript:;" data-url="/admin/tax/initManagerTaxList"
                   data-parent="true" data-title="税务审核">
                    <i class="iconfont " data-icon='&#xe63f;'></i>
                    <span style="font-size: 18px;">税务审核</span>
                </a>
            </div>
        </section>
    </div>
    </c:if>

    <%--纳税人快捷操作入口--%>
    <c:if test="${roleId == 0}">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
            <section class="panel">
                <div class="symbol bgcolor-yellow"><i class="iconfont">&#xe669;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/tax/initAddTax"
                       data-parent="true" data-title="申报税务">
                        <i class="iconfont " data-icon='&#xe669;'></i>
                        <span style="font-size: 18px;">申报税务</span>
                    </a>
                </div>
            </section>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
            <section class="panel">
                <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe61a;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/tax/initTaxList"
                       data-parent="true" data-title="税务列表">
                        <i class="iconfont " data-icon='&#xe61a;'></i>
                        <span style="font-size: 18px;">税务列表</span></a>
                </div>
            </section>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
            <section class="panel">
                <div class="symbol bgcolor-dark-green"><i class="iconfont">&#xe60a;</i>
                </div>
                <div class="value tab-menu" style="margin-top: 2em">
                    <a href="javascript:;" data-url="/admin/cert/initCerList"
                       data-parent="true" data-title="申请证书">
                        <i class="iconfont " data-icon='&#xe60a;'></i>
                        <span style="font-size: 18px;">申请证书</span></a>
                </div>
            </section>
        </div>
        </c:if>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <section class="panel">
                <div class="panel-heading">
                    基于PKI的税务服务平台
                    <a href="javascript:;" class="pull-right panel-toggle"><i class="iconfont">&#xe604;</i></a>
                </div>
                <div class="panel-body" style="padding: 20px;">
                    <p class="paragraph">本税务网站是一个安全的税务网站，数据交换过程中运用了数据加密、数字签名、数字信封等PKI技术。</p>
                    <p class="paragraph">由于网站开发或者能力有限，如果遇到使用上的问题，请联系我们。联系方式tax@163.com.</p>
                </div>
            </section>
        </div>
    </div>

    <%--<div class="row">--%>
    <%--<div class="col-md-12">--%>
    <%--<img src="/static/admin/images/banner.jpg" style="width: 100%;"/>--%>
    <%--</div>--%>
    <%--</div>--%>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use(['main']);
    // layui.use('list');
</script>

</html>