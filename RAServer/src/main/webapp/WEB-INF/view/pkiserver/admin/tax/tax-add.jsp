<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding">
                <form id="form1" class="layui-form layui-form-pane" data-render="true"
                      action="/admin/tax/addTax" method="post">

                    <div class="layui-form-item">
                        <label class="layui-form-label">收入</label>
                        <div class="layui-input-inline">
                            <input type="text" name="income" id="income" required jq-verify="number"
                                   jq-error="请输入金额|请输入正确的金额" placeholder="金额" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">税务类型</label>
                        <div class="layui-input-inline">
                            <select name="taxTypeId" jq-verify="required" jq-error="请选择税务类型">
                                <option value="">请选择</option>
                                <c:if test="${not empty taxTypeList }">
                                    <c:forEach var="taxType" items="${taxTypeList}" varStatus="status">
                                        <option value="${taxType.id}">${taxType.typeName}：${taxType.taxRate}</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>

                    <%--<div class="layui-form-item">--%>
                    <%--<label class="layui-form-label">税费</label>--%>
                    <%--<div class="layui-input-inline">--%>
                    <%--<input type="text" name="tax" id="tax" disabled="disabled" jq-verify="required|number" jq-error="请输入正确的税费"  placeholder="税费" autocomplete="off" class="layui-input layui-disabled">--%>
                    <%--</div>--%>
                    <%--</div>--%>

                    <div class="layui-form-item">
                        <label class="layui-form-label">上传图片</label>
                        <div class="layui-input-block">
                            <input type="file" name="file" lay-type="images" class="layui-upload-file" id="attach">
                            <input type="hidden" name="attachUrl" jq-verify="required" jq-error="请上传图片"
                                   error-id="img-error">
                            <p id="img-error" class="error"></p>
                        </div>
                        <div class="layui-input-block">
                            <div class="imgbox">
                                <img name="attachUrl" src="/static/admin/images/logo-color.png" alt="暂无" class="img-thumbnail">
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">PFX证书</label>
                        <div class="layui-input-block">
                            <input type="file" name="file" lay-type="file" class="layui-upload-file" id="tax">
                            <input type="hidden" name="privateKeyUrl" jq-verify="required" jq-error="请上传私钥"
                                   error-id="img-error">
                            <p class="error"></p>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">证书密码</label>
                        <div class="layui-input-inline">
                            <input type="password" name="password" id="password" required jq-verify="required"
                                   jq-error="请输入证书密码" placeholder="证书密码" autocomplete="off" class="layui-input ">
                        </div>
                    </div>

                    <label class="layui-form-label">申报内容</label>

                    <div class="layui-form-item layui-form-text">
                        <div class="layui-input-block">
                            <textarea name="content" jq-verify="content" id="content" style="display:none;"></textarea>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');
</script>

</html>