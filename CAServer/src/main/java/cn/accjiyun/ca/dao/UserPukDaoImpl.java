package cn.accjiyun.ca.dao;

import cn.accjiyun.ca.common.dao.DaoSupport;
import cn.accjiyun.ca.common.entity.PageModel;
import cn.accjiyun.ca.common.entity.QueryEntity;
import cn.accjiyun.ca.entity.UserPuk;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/5/23.
 */
@Repository("userPukDao")
public class UserPukDaoImpl extends DaoSupport<UserPuk> implements UserPukDao {
    /**
     * 创建用户公钥证书
     *
     * @param userPuk 用户公钥证书实体
     * @return 用户公钥证书ID
     */
    @Override
    public int createUserPuk(UserPuk userPuk) {
        save(userPuk);
        return userPuk.getUserId();
    }

    /**
     * 通过用户公钥证书ID数组删除用户公钥证书
     *
     * @param userPukIds 用户公钥证书ID数组
     */
    @Override
    public void deleteUserPuk(int[] userPukIds) {
        for (int i = 0; i < userPukIds.length; i++) {
            delete(userPukIds[i]);
        }
    }

    /**
     * 通过用户公钥证书ID查询用户公钥证书信息
     *
     * @param userPukId 庭成员ID
     * @return 用户公钥证书实体
     */
    @Override
    public UserPuk queryUserPukById(int userPukId) {
        return get(userPukId);
    }

    /**
     * 分页查询用户公钥证书信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 用户公钥证书实体列表
     */
    @Override
    public List<UserPuk> queryUserPukPage(QueryEntity queryEntity, PageModel<UserPuk> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取用户公钥证书总数
     *
     * @return 用户公钥证书总数
     */
    @Override
    public long queryAllUserPukCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找用户公钥证书信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 用户公钥证书实体列表
     */
    @Override
    public List<UserPuk> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 更新用户公钥证书信息
     *
     * @param userPuk 用户公钥证书实体
     */
    @Override
    public void updateUserPuk(UserPuk userPuk) {
        saveOrUpdate(userPuk);
    }
}
