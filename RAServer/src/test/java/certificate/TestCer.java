package certificate;

import cn.accjiyun.pki.controller.tax.AdminTaxController;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jiyun on 2017/5/25.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestCer {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestCer.class);

    public void getRootCer() {
        LOGGER.info(AdminTaxController.class.getResource("root.cer").getPath());
    }
}
