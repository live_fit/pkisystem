package cn.accjiyun.pki.entity;

import java.io.Serializable;

/**
 * Created by jiyun on 2017/5/23.
 */
public class CAConfig implements Serializable {

    // 颁发者
    public final static String CA_ROOT_ISSUER = "C=CN,ST=GX,L=GL,O=GUET,OU=HJY,CN=CA";
    //算法
    public final static String ALGORITHM = "RSA";
    //签名算法
    public final static String CA_ALGORITHM = "SHA256WithRSAEncryption";
    //哈希算法
    public final static String HASH = "SHA";
    //签名哈希算法
    public final static String SHA1WITHRSA = "SHA1withRSA";
    //证书类型
    public final static String PKCS12 = "PKCS12";
    //使用着
    private String CA_DEFAULT_SUBJECT = "C=CN,ST=GX,L=GL,O=GUET,OU=HJY,CN=";
    //别名
    private String PK_ALIAS = "szba";
    //私钥key
    private String PK_PASSWORD = "123456";


    public String getCA_DEFAULT_SUBJECT() {
        return CA_DEFAULT_SUBJECT;
    }

    public void setCA_DEFAULT_SUBJECT(String CA_DEFAULT_SUBJECT) {
        this.CA_DEFAULT_SUBJECT = CA_DEFAULT_SUBJECT;
    }

    public String getPK_ALIAS() {
        return PK_ALIAS;
    }

    public void setPK_ALIAS(String PK_ALIAS) {
        this.PK_ALIAS = PK_ALIAS;
    }

    public String getPK_PASSWORD() {
        return PK_PASSWORD;
    }

    public void setPK_PASSWORD(String PK_PASSWORD) {
        this.PK_PASSWORD = PK_PASSWORD;
    }
}
