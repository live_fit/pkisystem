
layui.define(['jquery', 'laydate'], function(exports) {
    var $ = layui.jquery,
        laydate = layui.laydate,
        start = {
            istime: true,
            istoday: false,
            fomat: 'YYYY-MM-DD hh:mm',
            choose: function(datas) {
                end.min = datas;
                end.start = datas
            }
        },

        end = {
            istime: true,
            istoday: false,
            fomat: 'YYYY-MM-DD hh:mm',
            choose: function(datas) {
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        },
        birth = {
            istime: true,
            fomat: 'YYYY-MM-DD hh:mm'
        };

    $('.start-date').click(function() {
        start.elem = this;
        laydate(start);
    })

    $('.end-date').click(function() {
        end.elem = this;
        laydate(end);
    })

    $('.birth-date').click(function() {
        birth.elem = this;
        laydate(birth);
    })

    exports('jqdate', {});
});