package cn.accjiyun.pki.dao;

import cn.accjiyun.pki.common.dao.BaseDao;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.entity.TaxType;

import java.util.List;

/**
 * Created by jiyun on 2017/5/18.
 */
public interface TaxTypeDao extends BaseDao<TaxType> {

    /**
     * 创建税务类型
     * @param taxType 税务类型实体
     * @return 税务类型ID
     */
    public int createTaxType(TaxType taxType);

    /**
     * 通过税务类型ID数组删除税务类型
     * @param taxTypeIds 税务类型ID数组
     */
    public void deleteTaxTypeById(int[] taxTypeIds);

    /**
     * 通过税务类型ID查询税务类型信息
     * @param taxTypeId 庭成员ID
     * @return 税务类型实体
     */
    public TaxType queryTaxTypeById(int taxTypeId);

    /**
     * 通过角色名称查询对应ID
     * @param roleName 角色名称
     * @return 角色ID
     */
    public int queryTaxTypeIdByName(String roleName);

    /**
     * 分页查询税务类型信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 税务类型实体列表
     */
    public List<TaxType> queryTaxTypePage(QueryEntity queryEntity, PageModel<TaxType> model);

    /**
     * 更新税务类型信息
     * @param taxType 税务类型实体
     */
    public void updateTaxType(TaxType taxType);

    /**
     * 获取税务类型总数
     * @return 税务类型总数
     */
    public long queryAllTaxTypeCount();

    /**
     * 利用hql语句查找税务类型信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 税务类型实体列表
     */
    public List<TaxType> createQuery(final String hql, final Object[] queryParams);
}
