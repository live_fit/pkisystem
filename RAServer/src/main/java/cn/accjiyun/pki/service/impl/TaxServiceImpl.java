package cn.accjiyun.pki.service.impl;

import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.dao.PacketsDao;
import cn.accjiyun.pki.dao.TaxDao;
import cn.accjiyun.pki.dao.TaxTypeDao;
import cn.accjiyun.pki.entity.Tax;
import cn.accjiyun.pki.entity.TaxType;
import cn.accjiyun.pki.service.TaxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jiyun on 2017/5/17.
 */
@Service("taxService")
public class TaxServiceImpl implements TaxService {

    @Autowired
    private TaxDao taxDao;
    @Autowired
    private TaxTypeDao taxTypeDao;

    /**
     * 创建税务账单
     *
     * @param tax 税务账单实体
     * @return 税务账单ID
     */
    @Override
    public int createTax(Tax tax) {
        return taxDao.createTax(tax);
    }

    /**
     * 通过税务账单ID数组删除税务账单
     *
     * @param taxIds 税务账单ID数组
     */
    @Override
    public void deleteTaxTypeById(int[] taxIds) {
        taxDao.deleteTax(taxIds);
    }

    /**
     * 通过税务账单ID查询税务账单信息
     *
     * @param taxId 庭成员ID
     * @return 税务账单实体
     */
    @Override
    public Tax queryTaxById(int taxId) {
        return taxDao.queryTaxById(taxId);
    }

    /**
     * 分页查询税务账单信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 税务账单实体列表
     */
    @Override
    public List<Tax> queryTaxPage(QueryEntity queryEntity, PageModel<Tax> model) {
        return taxDao.queryTaxPage(queryEntity, model);
    }

    /**
     * 通过ID查询税务类型
     *
     * @param id 税务类型ID
     * @return 税务类型实体
     */
    @Override
    public TaxType queryTaxTypeById(int id) {
        return taxTypeDao.queryTaxTypeById(id);
    }

    /**
     * 通过名称查询税务类型
     *
     * @param typeName 税务类型名称
     * @return 税务类型ID
     */
    @Override
    public int queryTaxTypeIdByName(String typeName) {
        return taxTypeDao.queryTaxTypeIdByName(typeName);
    }

    /**
     * 查询所有税务类型
     *
     * @return 税务类型列表
     */
    @Override
    public List<TaxType> queryAllTaxType() {
        return taxTypeDao.createQuery("from TaxType", new Object[0]);
    }

    /**
     * 更新税务账单信息
     *
     * @param tax 税务账单实体
     */
    @Override
    public void updateTax(Tax tax) {
        taxDao.updateTax(tax);
    }

    /**
     * 获取税务账单总数
     *
     * @return 税务账单总数
     */
    @Override
    public long queryAllTaxCount() {
        return taxDao.queryAllTaxCount();
    }

    /**
     * 利用hql语句查找税务账单信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 税务账单实体列表
     */
    @Override
    public List<Tax> createQuery(String hql, Object[] queryParams) {
        return taxDao.createQuery(hql, queryParams);
    }
}
