layui.define(['jquery', 'dtable', 'jqdate', 'jqform'], function (exports) {
    var $ = layui.jquery,
        list = layui.dtable,
        ajax = layui.ajax,
        laydate = layui.laydate,
        form = layui.jqform,
        oneList = new list();
    form.set({
        "change": true,
        "form": "#form1"
    }).init(oneList);

    form.abc = function (ret, options, that) {
        alert("这是form回调方法");
    }
    ajax.test = function (ret, options, that) {
        alert("这是ajax.test回调方法");
    }

    ajax.search = function (ret, options, that) {
        $("#search").click();
        // alert("这是ajax.search回调方法");
    }
    oneList.init('list-tpl');

    exports('list', {});
});
