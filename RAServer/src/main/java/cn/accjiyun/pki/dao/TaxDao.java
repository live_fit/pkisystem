package cn.accjiyun.pki.dao;

import cn.accjiyun.pki.common.dao.BaseDao;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.entity.Tax;

import java.util.List;

/**
 * Created by jiyun on 2017/5/17.
 */
public interface TaxDao extends BaseDao<Tax> {

    /**
     * 创建税务账单
     * @param tax 税务账单实体
     * @return 税务账单ID
     */
    public int createTax(Tax tax);

    /**
     * 通过税务账单ID数组删除税务账单
     * @param taxIds 税务账单ID数组
     */
    public void deleteTax(int[] taxIds);

    /**
     * 通过税务账单ID查询税务账单信息
     * @param taxId 庭成员ID
     * @return 税务账单实体
     */
    public Tax queryTaxById(int taxId);

    /**
     * 分页查询税务账单信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 税务账单实体列表
     */
    public List<Tax> queryTaxPage(QueryEntity queryEntity, PageModel<Tax> model);

    /**
     * 更新税务账单信息
     * @param tax 税务账单实体
     */
    public void updateTax(Tax tax);

    /**
     * 获取税务账单总数
     * @return 税务账单总数
     */
    public long queryAllTaxCount();

    /**
     * 利用hql语句查找税务账单信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 税务账单实体列表
     */
    public List<Tax> createQuery(final String hql, final Object[] queryParams);
}