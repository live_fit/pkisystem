package cn.accjiyun.pki.dao;

import cn.accjiyun.pki.common.dao.BaseDao;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.entity.UserRole;

import java.util.List;

/**
 * Created by jiyun on 2017/5/17.
 */
public interface UserRoleDao extends BaseDao<UserRole> {

    /**
     * 创建用户角色
     * @param userRole 用户角色实体
     * @return 用户角色ID
     */
    public int createUserRole(UserRole userRole);

    /**
     * 通过用户角色ID数组删除用户角色
     * @param userRoleIds 用户角色ID数组
     */
    public void deleteUserRole(int[] userRoleIds);

    /**
     * 通过用户角色ID查询用户角色信息
     * @param userRoleId 庭成员ID
     * @return 用户角色实体
     */
    public UserRole queryUserRoleById(int userRoleId);

    /**
     * 通过角色名称查询对应ID
     * @param roleName 角色名称
     * @return 角色ID
     */
    public int queryUserRoleIdByName(String roleName);

    /**
     * 分页查询用户角色信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 用户角色实体列表
     */
    public List<UserRole> queryUserRolePage(QueryEntity queryEntity, PageModel<UserRole> model);

    /**
     * 更新用户角色信息
     * @param userRole 用户角色实体
     */
    public void updateUserRole(UserRole userRole);

    /**
     * 获取用户角色总数
     * @return 用户角色总数
     */
    public long queryAllUserRoleCount();

    /**
     * 利用hql语句查找用户角色信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 用户角色实体列表
     */
    public List<UserRole> createQuery(final String hql, final Object[] queryParams);
}
