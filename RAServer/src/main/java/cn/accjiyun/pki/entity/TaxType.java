package cn.accjiyun.pki.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/6/1.
 */
@Entity
@Table(name = "tax_type", schema = "pki_system_db", catalog = "")
public class TaxType implements Serializable {
    private int id;
    private String typeName;
    private Double taxRate;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type_name", nullable = true, length = 20)
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Basic
    @Column(name = "tax_rate", nullable = true, precision = 0)
    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaxType taxType = (TaxType) o;

        if (id != taxType.id) return false;
        if (typeName != null ? !typeName.equals(taxType.typeName) : taxType.typeName != null) return false;
        if (taxRate != null ? !taxRate.equals(taxType.taxRate) : taxType.taxRate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
        result = 31 * result + (taxRate != null ? taxRate.hashCode() : 0);
        return result;
    }
}
