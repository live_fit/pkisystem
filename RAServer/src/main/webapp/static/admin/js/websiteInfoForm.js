/*
 * @Author: Paco
 * @Date:   2017-02-15
 * +----------------------------------------------------------------------
 * | jqadmin [ jq酷打造的一款懒人后台模板 ]
 * | Copyright (c) 2017 http://jqadmin.jqcool.net All rights reserved.
 * | Licensed ( http://jqadmin.jqcool.net/licenses/ )
 * | Author: Paco <admin@jqcool.net>
 * +----------------------------------------------------------------------
 */

layui.define(['jquery', 'element', 'jqform', 'upload'], function (exports) {
    var $ = layui.jquery,
        box = "",
        form = layui.jqform;
    form.set({
        "blur": true,
        "form": "#form1",
        "change": true
    }).init();

    //上传文件设置
    layui.upload({
        url: '/admin/upload/file?param=websiteInfo',
        before: function (input) {
            box = $(input).parent('form').parent('div').parent('.layui-input-block');
            if (box.next('div').length > 0) {
                box.next('div').html('<div class="imgbox"><p>上传中...</p></div>');
            } else {
                box.after('<div class="layui-input-block"><div class="imgbox"><p>上传中...</p></div></div>');
            }
        },
        success: function (res) {
            if (res.status == 200) {
                showImage.src = res.url;
                imageUrl.value = res.url;
            } else {
                box.next('div').find('p').html('上传失败...')
            }
        }
    });
    exports('websiteInfoForm', {});
});