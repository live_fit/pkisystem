<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/base.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title>基于PKI的税务服务平台</title>
    <link rel="stylesheet" href="/static/admin/css/pintuer.css">
    <link rel="stylesheet" href="/static/admin/css/admin.css">
    <script src="/static/admin/js/jquery.js"></script>
    <script src="/static/admin/js/pintuer.js"></script>
    <script type="text/javascript">
        $(function () {
            if (document.getElementById("msg").innerText == "") {
                document.getElementById("alert").style.visibility = "hidden";
            } else {
                document.getElementById("alert").style.visibility = "visible";
            }
        });
    </script>
</head>
<body>
<div class="bg"></div>
<div class="container">
    <div class="line bouncein">
        <div class="xs6 xm4 xs3-move xm4-move">
            <div style="height:1%;"></div>
            <div class="media media-y margin-big-bottom"/>
            <form action="/admin/main/addUser" method="post" id="loginForm">
                <div class="panel loginbox">
                    <div class="text-center margin-big padding-big-top">
                        <h1>新用户注册</h1>
                    </div>
                    <div class="panel-body" style="padding:30px; padding-bottom:10px; padding-top:10px;">
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="text" class="input input-big" name="systemUser.loginName"
                                       placeholder="登录账号" data-validate="required:请填写账号"/>
                                <span class="icon icon-user margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="password" class="input input-big" name="systemUser.password"
                                       placeholder="登录密码" data-validate="required:请填写密码"/>
                                <span class="icon icon-key margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="password" class="input input-big" placeholder="确认密码"
                                       data-validate="required:请填写密码,repeat#systemUser.password:两次输入的密码不一致，请检查"/>
                                <span class="icon icon-key margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="text" class="input input-big" name="systemUser.username"
                                       placeholder="姓名" data-validate="required:请填写姓名,chinese:请输入正确姓名"/>
                                <span class="icon icon-user margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="text" class="input input-big" name="systemUser.identityCard"
                                       data-validate="required:请填写身份证号码,number:请输入18位身份证号码" placeholder="输入身份证号码"/>
                                <span class="icon icon-bookmark margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field field-icon-right">
                                <input type="text" class="input input-big" name="systemUser.mobile"
                                       data-validate="required:请填写手机号,mobile:请输入正确的手机号" placeholder="输入手机号码"/>
                                <span class="icon icon-mobile margin-small"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field">
                                <input type="text" class="input input-big" name="randomCode" placeholder="填写右侧的验证码"
                                       data-validate="required:请填写右侧的验证码"/>
                                <span class="yzm-pic">
                                <img src="/ran/random" alt="验证码，点击图片更换" width="100" height="42" class="passcode"
                                     style="height:43px;cursor:pointer;"
                                     onclick="this.src='/ran/random?random='+Math.random();"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="padding:30px; padding-bottom:5px; padding-top:10px;">
                        <input type="submit" class="button button-block bg-main text-big input-big login" value="注册">
                        <div class="alert alert-red" id="alert">
                            <span class="close rotate-hover"></span><strong>注意：</strong>
                            <span id="msg">${message}</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>