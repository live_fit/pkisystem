<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>

<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!--头部搜索-->
            <section class="panel panel-padding">
                <form class="layui-form" action="/admin/system/getUserList?pageNo=1">
                    <div class="layui-form">
                        <div class="layui-inline">
                            <select name="roleId" lay-filter="ajax"
                                    data-params='{"url": "/admin/system/getUserList?pageNo=1","loading":"false", "confirm":"true", "complete":"search"}'>
                                <option value="-1">请选择角色</option>
                                <option value="0">申报人</option>
                                <option value="1">承办人</option>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <select name="disabled" lay-filter="ajax"
                                    data-params='{"url": "/admin/system/getUserList?pageNo=1","loading":"false", "confirm":"true", "complete":"search"}'>
                                <option value="-1">请选择状态</option>
                                <option value="0">启用</option>
                                <option value="1">禁用</option>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input class="layui-input" name="keyword" placeholder="登录名、姓名">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button lay-submit id="search" class="layui-btn" lay-filter="search">查找</button>
                        </div>
                    </div>
                </form>
            </section>

            <!--列表-->
            <section class="panel panel-padding">
                <div class="group-button">
                    <button class="layui-btn layui-btn-small layui-btn-danger ajax-all" data-name="id"
                            data-params='{"url": "/admin/system/delete","loading":"true"}'>
                        <i class="iconfont">&#xe626;</i> 删除
                    </button>
                    <button class="layui-btn layui-btn-small modal-iframe"
                            data-params='{"content": "/admin/system/userAdd", "title": "添加系统用户","full":"true"}'>
                        <i class="iconfont">&#xe649;</i> 添加
                    </button>
                </div>
                <div id="list" class="layui-form"></div>

                <div class="text-right" id="page"></div>
            </section>
        </div>
    </div>
</div>

<div class="add-subcat">
    <form id="form1" class="layui-form layui-form-pane" data-name="systemUserData" data-tpl="list-tpl"
          data-render="true" action="/admin/system/update" method="post">

        <div class="layui-form-item">
            <label class="layui-form-label">登录账号</label>
            <div class="layui-input-block">
                <input type="text" name="loginName" required jq-verify="required|validateLoginName" jq-error="请输入帐号|用户已存在"
                       placeholder="请输入登录账号" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">用户姓名</label>
            <div class="layui-input-block">
                <input type="text" name="username" required jq-verify="required" jq-error="请输入姓名"
                       placeholder="请输入姓名" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">身份证</label>
            <div class="layui-input-block">
                <input type="text" name="identityCard" required jq-verify="required" jq-error="请输入身份证"
                       placeholder="请输入身份证" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">手机</label>
            <div class="layui-input-block">
                <input type="text" name="mobile" required jq-verify="required" jq-error="请输入手机号"
                       placeholder="请输入手机号" autocomplete="off" class="layui-input ">
            </div>
        </div>

        <div class="layui-form-item" pane>
            <label class="layui-form-label">角色</label>
            <div class="layui-input-block">
                <input type="radio" name="roleId" title="申报人" value="0" checked/>
                <input type="radio" name="roleId" title="承办人" value="1"/>
            </div>
        </div>

        <div class="layui-form-item" pane>
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="radio" name="disabled" title="启用" value="0" checked/>
                <input type="radio" name="disabled" title="禁用" value="1"/>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" jq-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

</body>
<%@include file="/WEB-INF/layouts/admin/sys-user.jsp" %>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>

<script>
    layui.use('list');
    layui.use('myform');

</script>

</html>