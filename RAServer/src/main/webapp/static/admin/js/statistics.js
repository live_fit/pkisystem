var outputCharts = echarts.init(document.getElementById('output'));

outputCharts.setOption({
    title: {
        text: '产品产值比',
        subtext: '各产品类型的产值占总产值的百分比',
        x: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: []
    },
    series: [{
        name: '产品产值比',
        type: 'pie',
        radius: '55%',
        //          roseType: 'angle',
        center: ['50%', '60%'],
        data: [],
        itemStyle: {
            emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    }]
});

outputCharts.showLoading();
// 异步加载数据
$.get('/admin/production/outputCharts.json').done(function (json) {
    outputCharts.hideLoading();
    // 填入数据
    outputCharts.setOption({
        series: [{
            // 根据名字对应到相应的系列
            data: json.data
        }],
        legend: {
            data: json.legend.data
        }
    });
});


var yieldChart = echarts.init(document.getElementById('yield'));
// 指定图表的配置项和数据
yieldChart.setOption({

    tooltip: {
        trigger: 'axis'
    },
    //	color: ["#FF0000", "#00BFFF", "#FF00FF", "#1ce322", "#000000", '#EE7942'],
    legend: {
        data: []
    },
    grid: {
        left: '3%',
        right: '8%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        name: '年份',
        type: 'category',
        boundaryGap: false,
        data: []
    },
    yAxis: [{
        name: '产量(kg)',
        type: 'value',
        axisLabel: {
            formatter: '{value} '
        },
    }],
    dataZoom: [{
        type: 'inside',
        start: 0,
        end: 100
    }, {
        show: true,
        type: 'slider',
        y: '90%',
        start: 50,
        end: 100
    }],
    series: []
});
yieldChart.showLoading();
// 异步加载数据
$.get('/admin/production/yieldChart.json').done(function (json) {
    yieldChart.hideLoading();
    // 填入数据
    yieldChart.setOption({
        legend: {
            data: json.legend.data
        },
        xAxis: {
            data: json.xAxis.data
        },
        series: json.series
    });
});

var priceChart = echarts.init(document.getElementById('price'));
priceChart.setOption({

    tooltip: {
        trigger: 'axis'
    },
    //	color: ["#FF0000", "#00BFFF", "#FF00FF", "#1ce322", "#000000", '#EE7942'],
    legend: {
        data: []
    },
    grid: {
        left: '3%',
        right: '8%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        name: '年份',
        type: 'category',
        boundaryGap: false,
        data: []
    },
    yAxis: [{
        name: '价格(元/kg)',
        type: 'value',
        axisLabel: {
            formatter: '{value} '
        },
    }],
    dataZoom: [{
        type: 'inside',
        start: 0,
        end: 100
    }, {
        show: true,
        type: 'slider',
        y: '90%',
        start: 50,
        end: 100
    }],
    series: []
});
priceChart.showLoading();
// 异步加载数据
$.get('/admin/production/priceChart.json').done(function (json) {
    priceChart.hideLoading();
    // 填入数据
    priceChart.setOption({
        legend: {
            data: json.legend.data
        },
        xAxis: {
            data: json.xAxis.data
        },
        series: json.series
    });
});